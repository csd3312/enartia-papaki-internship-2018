


Vue.component('photoComponent',{
    props:['photo'],
    template: 'photo.html'
});

var app= new Vue({
    el:'#app',
    data:{photos:''},
    mounted () {
        axios
          .get('https://jsonplaceholder.typicode.com/photos')
          .then(response => {
              this.photos = response.data;
              console.log(response);
          }  )
      }
});