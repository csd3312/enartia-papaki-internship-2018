var app = new Vue(
    {
        el: '#app',  //el stands for element
        data: {      // declaring data
            message: 'Hello Vue'
        }
    }
);

var app2 = new Vue({
    el: '#app-2',
    data: {
        message: 'You loaded this page on ' + new Date().toLocaleString()
    }
});


var app3 = new Vue({
    el: '#app-3',
    data: {
        seen: true
    }
});

var app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [  //array syntax
            { text: 'Go for a walk' },
            { text: 'Pay the bank' },
            { text: 'Finish my Vue tutorial' }
        ]
    },
    mounted () {
        axios
          .get('https://jsonplaceholder.typicode.com/photos')
          .then(response => {
              console.log("asda");
              console.log(response);

              var array = [];
              array = response.data;
              
            var i;
            for(i=0;i<array.length;i++){
                console.log(array[i].title);
            }

          })
      }
});


var app5 = new Vue({
    el: '#app-5',
    data: {
        message: 'Hello Vue.js!'
    },
    methods: {  //declaring application methods
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
});


var app6 = new Vue({
    el: '#app-6',
    data: {
        message: 'Hey man'
    }
});

//declaring a component for the first time!
Vue.component('alex-selector', { //the first arg refers to the element's selector name in DOM
    props: ['todo'], //props are like custom attributes
    template: '<li>{{todo.text}}</li>'
}); // template holds the html code of our component

//so here we are having our component ready.

var app7 = new Vue({
    el: '#app-7',
    data: {
        studyList: [
            { id: 1, text: 'Vue' },
            { id: 2, text: 'React' },
            { id: 3, text: 'Angular' }
        ]
    }
});