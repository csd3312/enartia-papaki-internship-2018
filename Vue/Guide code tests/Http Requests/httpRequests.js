

var app= new Vue({
    el:'#app',
    data:{response:''},
    mounted () {
        axios
          .get('https://jsonplaceholder.typicode.com/photos',{headers: {
            'Access-Control-Allow-Origin': '*',
          },})
          .then(response => {
              this.response = response;
          }  )
      }
});