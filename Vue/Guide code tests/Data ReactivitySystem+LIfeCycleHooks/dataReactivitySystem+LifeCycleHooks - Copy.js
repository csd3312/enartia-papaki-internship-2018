var mydata = {attribute:'alex'};

var vm = new Vue({ //options object
    el:'#app',
    data: mydata,  // connecting mydata to Vue's instance data
    created: function(){
        console.log("The app was created!");
    },
    updated: function(){
        console.log("The app was updated!");
    }
});

console.log('Equality of mydata and vue instance data');
console.log(mydata.attribute == vm.attribute);

console.log('Changing vues instance data to rasi to see if mydata changes');
vm.attribute = 'rasi';
console.log('vm.attribute: '+vm.attribute);
console.log('mydata.attribute: '+mydata.attribute);

console.log('Changing mydata instance data to backToAlex to see if vues data changes');
mydata.attribute = 'backToAlex';
console.log('vm.attribute: '+vm.attribute);
console.log('mydata.attribute: '+mydata.attribute);


vm.newProperty = 'hey new newProperty';
console.log(vm.newProperty);

//Object.freeze() test

var obj = {
    foo:'bar'
};

Object.freeze(obj);

var objectFreezeApp = new Vue({
    el:'#objectFreezeApp',
    data:obj
})


console.log("TESTING INSTANCE PROPERTIES AND METHODS BELOW");
console.log(vm.$data === mydata);
console.log(vm.$el === document.getElementById('app'));

vm.$watch('attribute',function(newValue,oldValue){
    //this callback wil be called whn vm.attribute canges
    console.log("callback called! vm.attribute changed!");
});

