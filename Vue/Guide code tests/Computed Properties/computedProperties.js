

var app= new Vue({
    el:'#app',
    data:{
        message:'Enartia'
    },
    computed:{  // here goes thee computed properties object

        reversedMessage: function(){
            return this.message.split('').reverse().join('');
        },

        revCapMessage: function(){
            return this.reversedMessage.toUpperCase();
        }

    },
    methods:{
        reversedMessageFunc:function(){
            return this.message.split('').reverse().join('');
        }
    }
});


var watchApp = new Vue({
    el:'#watchApp',
    data:{
        firstName:'Alex',
        lastName: 'Rasidakis',
        fullName:'Alex Rasidakis'
     },
     watch:{
         firstName: function(value){
             this.fullName = value +' '+this.lastName;
         },
         lastName: function(value){
            this.fullName = this.firstName +' ' +value;
        }
     }
});


var computedApp = new Vue({
    el:'#computedApp',
    data:{
        firstName:'Alex',
        lastName:'Rasidakis'
    },
    computed:{
        fullName: function(){
            return this.firstName +' '+this.lastName;
        }
    }
})

var computedSetAndGet = new Vue({
    el:'#computedSetGet',
    data:{
        firstName:'Alex',
        lastName:'Rasidakis'
    },
    computed:{
        fullName:{
            //getter
            get: function(){
                return this.firstName+' '+this.lastName;
            },
            set: function(value){
                var splittedNames = value.split(' ');
                this.firstName = splittedNames[0];
                this.lastName = splittedNames[1];
            }
        }
    }
});


var watchersApp = new Vue({
    el:'#watch-example',
    data:{
        question:'',
        answer:' I cannot give you an answer until you ask a question!'
    },
    watch:{
        question:function(newQuestion,oldQuestion){
            this.answer = 'Waiting for you to stop typing'
            this.debouncedGetAnswer()
        }
    }
})